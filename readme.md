# 關於此專案
為臺北某護理學校登入後抓取班級名單功能
# 如何使用
第一次先運行以下指令
```bash=
npm install
```
接下來輸入
```bash
cd SchoolClassList
node test.js 學號 密碼
```
# 運行效果
![運行效果](運行效果.gif "運行效果")

# 運行結果
![運行結果](result.jpg "運行儲存之結果")
